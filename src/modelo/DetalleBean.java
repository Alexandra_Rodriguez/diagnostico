package modelo;

public class DetalleBean {

    private int num_detalle;
    private FacturaBean num_factura;
    private ProductoBean id_producto;
    private int cantidad;
    private double precio;

    public DetalleBean(int num_detalle) {
        this.num_detalle = num_detalle;
    }

    public int getNum_detalle() {
        return num_detalle;
    }

    public void setNum_detalle(int num_detalle) {
        this.num_detalle = num_detalle;
    }

    public FacturaBean getNum_factura() {
        return num_factura;
    }

    public void setNum_factura(FacturaBean num_factura) {
        this.num_factura = num_factura;
    }

    public ProductoBean getId_producto() {
        return id_producto;
    }

    public void setId_producto(ProductoBean id_producto) {
        this.id_producto = id_producto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

}
