package modelo;

import java.util.Date;

public class FacturaBean {

    private int num_factura;
    private ClienteBean id_cliente;
    private Date fecha;
    private Modo_PagoBean num_pago;

    public FacturaBean(int num_factura) {
        this.num_factura = num_factura;
    }

    public int getNum_factura() {
        return num_factura;
    }

    public void setNum_factura(int num_factura) {
        this.num_factura = num_factura;
    }

    public ClienteBean getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(ClienteBean id_cliente) {
        this.id_cliente = id_cliente;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Modo_PagoBean getNum_pago() {
        return num_pago;
    }

    public void setNum_pago(Modo_PagoBean num_pago) {
        this.num_pago = num_pago;
    }

}
