package modelo;

public class Modo_PagoBean {

    private int num_pago;
    private String nombre;
    private String otros_detalles;

    public Modo_PagoBean(int num_pago) {
        this.num_pago = num_pago;
    }

    public int getNum_pago() {
        return num_pago;
    }

    public void setNum_pago(int num_pago) {
        this.num_pago = num_pago;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getOtros_detalles() {
        return otros_detalles;
    }

    public void setOtros_detalles(String otros_detalles) {
        this.otros_detalles = otros_detalles;
    }

}
