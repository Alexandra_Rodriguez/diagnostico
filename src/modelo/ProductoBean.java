package modelo;

public class ProductoBean {

    private int id_producto;
    private String nombre;
    private double precio;
    private int stock;
    private CategoriaBean id_categoria;

    public ProductoBean(int id_producto) {
        this.id_producto = id_producto;
    }

    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public CategoriaBean getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(CategoriaBean id_categoria) {
        this.id_categoria = id_categoria;
    }

}
