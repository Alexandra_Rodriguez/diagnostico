create database diagnostico;
use diagnostico;

create table cliente(
id_cliente int primary key auto_increment not null,
nombre varchar(50) not null,
apellido varchar(50) not null,
direccion varchar(200) not null,
fecha_nacimiento date not null,
telefono varchar(13) not null,
email varchar(25) not null 
);

create table modo_pago(
num_pago int primary key auto_increment not null,
nombre varchar(50) not null,
otros_detalles varchar(250) not null 
);

create table categoria(
id_categoria int primary key auto_increment not null,
nombre varchar(50) not null,
descripcion varchar(500) not null
);

create table producto(
id_producto int primary key auto_increment not null,
nombre varchar(50) not null,
precio double(7,2) not null,
stock int(11) not null,
id_categoria int not null,
constraint producto_id_categoria foreign key(id_categoria) references categoria(id_categoria) on update cascade on delete cascade
);

create table factura(
num_factura int primary key auto_increment not null,
id_cliente int not null,
fecha date not null,
num_pago int not null,
constraint factura_id_cliente foreign key(id_cliente) references cliente(id_cliente) on update cascade on delete cascade,
constraint factura_num_pago foreign key(num_pago) references modo_pago(num_pago) on update cascade on delete cascade
);

create table detalle(
num_detalle int primary key auto_increment not null,
num_factura int not null,
id_producto int not null,
cantidad int(11) not null,
precio double(7,2) not null,
constraint detalle_num_factura foreign key(num_factura) references factura(num_factura) on update cascade on delete cascade,
constraint detalle_id_producto foreign key(id_producto) references producto(id_producto) on update cascade on delete cascade
);